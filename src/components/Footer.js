import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../assets/img/filmApp.svg';

const Footer = () => {
  return (
    <section className="footer">
        <div className="container">
            <div className="content-footer">
                <div className="derechos">
                    <p>&copy;filmapp.us 2019. Todos los derechos reservados.</p>
                </div>
                <div className="logo-footer">
                    <Link to="/">
                        <img src={Logo} alt="FilmApp Logotipo" /> 
                    </Link>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Footer;
