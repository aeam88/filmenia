import React, { Component } from 'react';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../config';
import { Link } from 'react-router-dom';

export class Card extends Component {

    nombreGenero = () => {
        const primerGenero = this.props.movie.genre_ids[0];
        const { generosArray } = this.props;

        if(generosArray.length === 0) {
            return null;
        } else {
            const arregloNombres = generosArray.filter( (genero) => {
                return genero.id === primerGenero;   
            });


            return arregloNombres[0].name;
        }
    }

  render() {
    return (
        <Link to={`/movie/${this.props.movie.id}`}  className="card-movie">
                <div className="img-movie">
                    <img src={this.props.movie.poster_path ? `${IMAGE_BASE_URL}${POSTER_SIZE}/${this.props.movie.poster_path}` : 'http://placehold.it/400x500'} alt="Caratula movie" />
                </div>
                <div className="info-movie">
                    <h2>{this.props.movie.title}</h2>
                    <p>{this.props.movie.release_date}</p>
                    <div className="mb-movie-info mt-movie-info">
                        <h4>GENERO</h4>
                        <p>{this.nombreGenero()}</p>
                    </div>
                    <div className="mb-movie-info">
                        <h4>POPULARIDAD</h4>
                        <p>{this.props.movie.popularity}</p>
                    </div>
                    <div className="mb-movie-info">
                        <h4>IDIOMA</h4>
                        <p>{this.props.movie.original_language}</p>
                    </div>
                    <div>
                        <p className="cate-movie-info">{this.props.movie.vote_average} / 10.0</p>
                    </div>
                </div>
        </Link>
    )
  }
}

export default Card;
