import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../assets/img/filmApp.svg';

const Header = () => {

    return (
        <header className="menu-nav">
        <div className="container">
            <nav className="main-nav">
                <div className="nav-izq-head">
                    <Link to="/">
                       <img src={Logo} alt="FilmApp Logotipo" />    
                    </Link>
                </div>
            </nav>
        </div>  
    </header>
    )
}

export default Header;
