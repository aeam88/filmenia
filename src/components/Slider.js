import React from 'react';

const Slider = ({ imageSlide, titMovie, releaseDate }) => {
  return (
    <section className="slider-main" style={{ backgroundImage: `url('${imageSlide}')` }}>
        <div className="movie-name">
            <div className="container">
                <div className="content-titulo">
                    <h3>{releaseDate}</h3>
                    <h1>{titMovie}</h1>
                </div>
            </div>
        </div>
    </section>
  )
}

export default Slider;
