import React, { Component, Fragment } from 'react';
import Slider from './Slider';
import Card from './Card';
import SearchMovie from './SearchMovie';
import axios from 'axios';

import { API_URL, API_KEY, IMAGE_BASE_URL, BACKDROP_SIZE } from '../config';

export class Home extends Component {

    state = {
        movies: [],
        heroImage: null,
        loading: false,
        currentPage: 0,
        totalPages: 0,
        searchWord: '',
        generosArray: []
    }

    async componentDidMount() {
        this.setState({
            loading: true
        });
        const endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=es&page=1`;
        this.fetchItems(endpoint);

        const generos = `${API_URL}genre/movie/list?language=es&api_key=${API_KEY}`;
        this.fetchGeneros(generos);
    }


    searchItems = async (searchWord) => {
        let endpoint = '';
        this.setState({
            movies: [],
            loading: true,
            searchWord
        });

        if(searchWord === '') {
            endpoint = `${API_URL}movie/popular?api_key=${API_KEY}&language=es&page=1`;
        } else {
            endpoint = `${API_URL}search/movie?api_key=${API_KEY}&language=es&query=${searchWord}`;
        }

        this.fetchItems(endpoint);
    }


    fetchItems = async (endpoint) => {

        await axios.get(endpoint)
                .then( result => {
                    this.setState({
                        movies: [...this.state.movies, ...result.data.results],
                        heroImage: this.state.heroImage || result.data.results[2],
                        loading: false,
                        currentPage: result.data.page,
                        totalPages: result.data.total_pages
                    })
                }).catch((err) => {
                    console.log(err)
                })
    }

    fetchGeneros = async (generos) => {

        await axios.get(generos)
            .then( result => {
                this.setState({
                    generosArray: result.data.genres
                })
            })
    }

    

    mostrarPeliculas = () => {
        const { movies } = this.state;
        if(movies.length === 0) return null;
        
        return (
            <React.Fragment>
                {this.state.movies.map(movie => (
                    <Card 
                        key={movie.id}
                        movie={movie}
                        generosArray={this.state.generosArray} />
                ))}
            </React.Fragment>
        );
    }

  render() {
    return (
        <Fragment>
            {this.state.heroImage !== null ? 
                <Slider 
                    imageSlide={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${this.state.heroImage.backdrop_path}`}
                    titMovie={this.state.heroImage.title}
                    releaseDate={this.state.heroImage.release_date}     
                /> : null
            }
            
            <section className="movies">
                <div className="container">
                    <SearchMovie callback={this.searchItems} />
                    <div className="contenedor-movies">
                        {this.mostrarPeliculas()}
                    </div>
                </div>
            </section>
        </Fragment>
    )
  }
}

export default Home;
