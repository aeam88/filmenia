import React, { Component } from 'react';

export class SearchMovie extends Component {
    state = {
        valor: ''
    }

    timeout = null;

    hacerBusqueda = (e) => {
        this.setState({
            valor: e.target.value
        });
        clearTimeout(this.timeout);

        this.timeout = setTimeout(() => {
            this.props.callback(this.state.valor);
        }, 500);
    }

  render() {
    return (
        <div className="search-container">
            <div className="search-box">
                <input 
                    type="text" 
                    className="input-search" 
                    name="buscar" 
                    placeholder="Buscar película..."
                    onChange={this.hacerBusqueda}
                    value={this.state.valor} />
                <i className="fas fa-search"></i>
            </div>
        </div>
    )
  }
}

export default SearchMovie;
