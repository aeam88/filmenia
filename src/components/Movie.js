import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { API_URL, API_KEY, IMAGE_BASE_URL, BACKDROP_SIZE } from '../config';

export class Movie extends Component {

  state = {
    movieInfo: {}
  }

  componentDidMount() {
    const { id } = this.props;
    const endpoint = `${API_URL}movie/${id}?api_key=${API_KEY}&language=es`;
        this.fetchMovie(endpoint);
  }

  fetchMovie = (endpoint) => {
    fetch(endpoint)
      .then( result => result.json())
      .then( result => {
          this.setState({
            movieInfo: result
          })
      })
  }

  render() {
    //console.log(this.state.movieInfo)
    return (
      <div className="container">
        <h2 className="tit-movie-individual">{this.state.movieInfo.title}</h2>
        <div className="card-movie-individual">
          <div className="img-movie">
              <img src={this.state.movieInfo.poster_path ? `${IMAGE_BASE_URL}${BACKDROP_SIZE}/${this.state.movieInfo.backdrop_path}` : 'http://placehold.it/1280x720'} alt="Caratula movie" />
          </div>
          <div className="info-movie-individual">
            <div className="mb-movie-info-2 mt-movie-info">
                <h3>TÍTULO ORIGINAL</h3>
                <p>{this.state.movieInfo.original_title}</p>
            </div>
            <div className="mb-movie-info-2 mt-movie-info">
                <h3>DURACIÓN</h3>
                <p>{this.state.movieInfo.runtime} min.</p>
            </div>
            <div className="mb-movie-info-2 mt-movie-info">
                <h3>PRESUPUESTO</h3>
                <p>$ {this.state.movieInfo.budget} USD.</p>
            </div>
            <div className="mb-movie-info-2 mt-movie-info">
                <h3>RESEÑA</h3>
                <p>{this.state.movieInfo.overview} min.</p>
            </div>
          </div>
        </div>
        <div className="contenedor-link-oficial">
          <Link to="/" className="btn-volver-home">Volver a todas las películas</Link>
          <a href={this.state.movieInfo.homepage} target="_blank" rel="noopener noreferrer" className="btn-link-oficial">Link página oficial</a>
        </div>
      </div>
    )
  }
}

export default Movie;

