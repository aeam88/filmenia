const API_URL = 'https://api.themoviedb.org/3/';
const API_KEY = 'fdf272afd6a41ef1e30e1014717093d4';

const IMAGE_BASE_URL = 'http://image.tmdb.org/t/p/';
const BACKDROP_SIZE = 'w1280';

const POSTER_SIZE = 'w500';

export {
    API_URL,
    API_KEY,
    IMAGE_BASE_URL,
    BACKDROP_SIZE,
    POSTER_SIZE
}