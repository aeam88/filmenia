import React, { Component, Fragment } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Header from './components/Header';
import Home from './components/Home';
import Movie from './components/Movie';
import Footer from './components/Footer';

import './css/App.css';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Fragment>
          <Header />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/movie/:id" render={ (props) => {
              let idMovie = props.location.pathname.replace('/movie/', '');
              return (
                <Movie
                  id={idMovie}
                />
              )
            }} />
          </Switch>
          <Footer />
        </Fragment>
      </BrowserRouter>
    );
  }
}

export default App;
